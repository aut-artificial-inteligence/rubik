public class Node {
    private Node parent;
    private int edgeTypeParent;
    private int[][] state;

    public Node(Node parent,int edgeTypeParent, int[][] state){
        this.parent = parent;
        this.edgeTypeParent = edgeTypeParent;
        this.state = new int[6][4];

        if (state!=null){
            for(int i=0 ; i<6 ; i++){
                for(int j=0 ; j<4 ; j++){
                    this.state[i][j] = state[i][j];
                }
            }
        }
    }

    public int[][] getState() {
        return state;
    }

    public Node getParent() {
        return parent;
    }


    public int getEdgeTypeParent() {
        return edgeTypeParent;
    }
}
