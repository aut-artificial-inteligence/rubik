import java.util.ArrayList;
import java.util.LinkedList;

public class UCSSolver extends Solver{
    private final int INITIAL_DEPTH_LIMIT = 0;
    private Node root;

    LinkedList<Node> frontier;
    ArrayList<int[][]> exploreSet;

    private int memoryNodeAllocatedMax;

    public UCSSolver(int[][] initialState){
        frontier = new LinkedList<Node>();
        exploreSet = new ArrayList<int[][]>();

        this.initialState = initialState;
        rubik = new Rubik();
    }


    public void solve(){
        root = new Node(null,-1,initialState);
        exploreSet.add(initialState);
        frontier.add(root);
        exploreNodes++;
        memoryNodeAllocatedMax = frontier.size();

        ArrayList<Integer> solution;
        while (true){
            try {
                solution = expand();
                if (solution != null){
                    System.out.println("fdfdfdf");
                    showSolution(solution);
                    return;
                }
            } catch (Exception e) {
                System.out.println("Solver can not find path to goal!");
                break;
            }
        }

    }

    public ArrayList<Integer> expand() throws Exception{
        Node node = frontier.pollFirst();
        if (node == null){
            throw new Exception();
        }
        else{
            rubik.setState(node.getState());
            if (rubik.isGoal()){
                return pathToRoot(node);
            }
            expandNodes++;
            for(int i=0 ; i<rubik.NUMBER_NEIGHBOUR ; i++){
                rubik.setState(node.getState());
                rubik.nextState(i);
                int[][] newState = rubik.getState();
                Node newNode = new Node(node,i,newState);
                if(!isExploredBefore(exploreSet,newState)){
                    exploreNodes++;
                    exploreSet.add(newState);
                    frontier.add(newNode);
                }

            }
            if (frontier.size() > memoryNodeAllocatedMax){
                memoryNodeAllocatedMax = frontier.size();
            }
            return null;
        }
    }

    @Override
    public void showSolutionMemory() {
        System.out.println("Maximum Node storage : "+ memoryNodeAllocatedMax);
        System.out.println("Maximum State storage: "+ exploreSet.size());
    }

}
