public class P1C_Main {
    public static void main(String args[]){
//        Solver solver = new UCSSolver(Testcase.test6());
//        solver.solve();

        int[][] state =  new int[][]{{6,2,4,3},{5,6,1,4},{1,1,5,3},{2,6,1,5},{3,4,2,3},{6,2,4,5}};

        Solver solver = new BidirectionalSolver(state);
        solver.solve();

    }
}
