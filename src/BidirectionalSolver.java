import java.util.ArrayList;
import java.util.LinkedList;

public class BidirectionalSolver extends Solver{
    private Node iRoot;
    private Node gRoot;

    LinkedList<Node> iFrontier;
    LinkedList<Node> gFrontier;
    ArrayList<int[][]> iExplore;
    ArrayList<int[][]> gExplore;

    private int memoryNodeAllocatedMax;

    public BidirectionalSolver(int[][] initialState){
        iFrontier = new LinkedList<Node>();
        gFrontier = new LinkedList<Node>();
        iExplore = new ArrayList<int[][]>();
        gExplore = new ArrayList<int[][]>();

        this.initialState = initialState;
        rubik = new Rubik();

    }

    public void solve(){
        iRoot = new Node(null,-1,initialState);
        iExplore.add(initialState);
        iFrontier.add(iRoot);
        exploreNodes++;

        gRoot = new Node(null,-1,null);
        int[][][] goalStates = rubik.getGoalStates();
        expandNodes++;
        for(int i=0 ; i<rubik.NUMBER_GOALS ; i++){
            int[][] goalState = goalStates[i];
            gExplore.add(goalState);
            gFrontier.add(new Node(gRoot,-1,goalState));
            exploreNodes++;
        }
        memoryNodeAllocatedMax = gFrontier.size() + iFrontier.size();

        ArrayList<Integer> solution;
        while (true){
            try {
                solution = expandInitial();
                if (solution != null){
                    System.out.println("qqqq");
                    showSolution(solution);
                    return;
                }
                solution = expandGoal();
                if (solution != null){
                    System.out.println("wwwww");
                    showSolution(solution);
                    return;
                }
            } catch (Exception e) {
                System.out.println("Solver can not find path to goal!");
                break;
            }
        }

    }

    public ArrayList<Integer> expandInitial() throws Exception{
        Node node = iFrontier.pollFirst();
        if (node == null){
            throw new Exception();
        }
        else{
            Node goalNode = containState(gFrontier,node.getState());
            if(goalNode!=null){
                return path(node,goalNode);
            }
            expandNodes++;
            for(int i=0 ; i<rubik.NUMBER_NEIGHBOUR ; i++){
                rubik.setState(node.getState());
                rubik.nextState(i);
                int[][] newState = rubik.getState();
                if(!isExploredBefore(iExplore,newState)){
                    Node newNode = new Node(node,i,newState);
                    iExplore.add(newState);
                    iFrontier.add(newNode);
                    exploreNodes++;
                }
            }
            if (gFrontier.size() + iFrontier.size() > memoryNodeAllocatedMax){
                memoryNodeAllocatedMax = gFrontier.size() + iFrontier.size();
            }
            return null;
        }
    }

    public ArrayList<Integer> expandGoal() throws Exception{
        Node node = gFrontier.pollFirst();
        if (node == null){
            throw new Exception();
        }
        else{
            Node initialNode = containState(iFrontier,node.getState());
            if(initialNode != null){
                return path(initialNode,node);
            }
            expandNodes++;
            for(int i=0 ; i<rubik.NUMBER_NEIGHBOUR ; i++){
                rubik.setState(node.getState());
                rubik.nextState(i);
                int[][] newState = rubik.getState();
                if(!isExploredBefore(gExplore,newState)){
                    Node newNode = new Node(node,i,newState);
                    gExplore.add(newState);
                    gFrontier.add(newNode);
                    exploreNodes++;
                }
            }
            if (gFrontier.size() + iFrontier.size() > memoryNodeAllocatedMax){
                memoryNodeAllocatedMax = gFrontier.size() + iFrontier.size();
            }
            return null;
        }
    }

    public ArrayList<Integer> path(Node initialNode, Node goalNode){
        ArrayList<Integer> reversePath = new ArrayList<Integer>();
        ArrayList<Integer> path = new ArrayList<Integer>();
        Node n = initialNode;
        while (n.getParent()!=null){
            reversePath.add(n.getEdgeTypeParent());
            n = n.getParent();
        }

        for(int i=reversePath.size() -1 ; i>=0 ; i--){
            path.add(reversePath.get(i));
        }

        n = goalNode;
        while (n.getParent() != gRoot){
            path.add(rubik.reverseClockwise(n.getEdgeTypeParent()));
            n = n.getParent();
        }
        return path;
    }

    public Node containState(LinkedList<Node> list, int[][] state){
        for(Node node:list){
            if(rubik.isStatesEqual(node.getState(),state)){
                return node;
            }
        }
        return null;
    }

    @Override
    public void showSolutionMemory() {
        System.out.println("Maximum Node storage : "+ memoryNodeAllocatedMax);
        System.out.println("Maximum State storage: "+ (iExplore.size() + gExplore.size()));
    }

}
