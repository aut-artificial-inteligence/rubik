public class Testcase {

    private static int[][] goalState(){
        return new int[][]{{1,1,1,1},{2,2,2,2},{3,3,3,3},{4,4,4,4},{5,5,5,5},{6,6,6,6}};
    }

    public static int[][] test1(){
        return goalState();
    }

    public static int[][] test2(){
        int[][] state = goalState();
        Rubik rubik = new Rubik();
        rubik.setState(state);
        rubik.rotate(1,true);
        return rubik.getState();
    }

    public static int[][] test3(){
        int[][] state = goalState();
        Rubik rubik = new Rubik();
        rubik.setState(state);
        rubik.rotate(1,true);
        rubik.rotate(2,false);
        return rubik.getState();
    }

    public static int[][] test4(){
        int[][] state = goalState();
        Rubik rubik = new Rubik();
        rubik.setState(state);
        rubik.rotate(1,false);
        rubik.rotate(2,false);
        rubik.rotate(3,false);
        return rubik.getState();
    }

    public static int[][] test5(){
        int [][]state = goalState();
        Rubik rubik = new Rubik();
        rubik.setState(state);
        rubik.nextState(1);
        rubik.nextState(4);
        rubik.nextState(7);
        rubik.nextState(9);
        rubik.nextState(3);
        return rubik.getState();
    }

    public static int[][] test6(){
        int[][] state = goalState();
        Rubik rubik = new Rubik();
        rubik.setState(state);
        rubik.nextState(2);
        rubik.nextState(8);
        rubik.nextState(3);
        rubik.nextState(11);
        rubik.nextState(1);
        rubik.nextState(5);
        return rubik.getState();
    }

    public static int[][] test7(){
        int [][]state = { {4,4,2,2},{1,3,5,6},{1,1,5,5},{6,3,6,3},{4,2,2,4},{3,1,6,5}} ;
        return state;
    }

}
