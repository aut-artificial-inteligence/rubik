import java.util.ArrayList;

public abstract class Solver {
    public Rubik rubik;
    public int[][] initialState;
    public int exploreNodes;
    public int expandNodes;


    public abstract void solve();

    public void showSolution(ArrayList<Integer> solution){
        System.out.println("Explore Nodes Number : "+exploreNodes);
        System.out.println("Expand Nodes Number : "+expandNodes);
        System.out.println("Depth Of Goal : " + solution.size());
        showSolutionMemory();
        rubik.setState(initialState);
        rubik.showState();
        for (int i=0 ; i<solution.size() ; i++){
            rubik.interpret(solution.get(i));
            rubik.nextState(solution.get(i));
            rubik.showState();

        }
    }

    public abstract void showSolutionMemory();

    public ArrayList<Integer> pathToRoot(Node node){
        ArrayList<Integer> reversePath = new ArrayList<Integer>();
        ArrayList<Integer> path = new ArrayList<Integer>();
        Node n = node;
        while (n.getParent()!=null){
            reversePath.add(n.getEdgeTypeParent());
            n = n.getParent();
        }

        for(int i=reversePath.size() -1 ; i>=0 ; i--){
            path.add(reversePath.get(i));
        }
        return path;
    }

    public boolean isExploredBefore(ArrayList<int[][]> list,int [][]state){
        for (int i=0 ; i<list.size() ; i++){
            if(rubik.isStatesEqual(list.get(i),state)){
                return true;
            }
        }
        return false;
    }

}
