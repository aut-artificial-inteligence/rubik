
public class Pair {
    public int left;
    public int right;

    public Pair(int left, int right){
        this.left = left;
        this.right = right;
    }

    public Pair reverse(){
        int tmp = right;
        right = left;
        left = tmp;

        return this;
    }

    @Override
    public String toString() {
        return "("+left+","+right+")";
    }
}
