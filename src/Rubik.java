import java.util.Arrays;
import java.util.List;

public class Rubik {
    private int state[][] ;
    public final int NUMBER_NEIGHBOUR = 12;
    public final int NUMBER_GOALS = 720;
    private int[][][] goalStates;
    private int numberGoalState;

    public Rubik() {
        state = new int[6][4];
    }

    public void setState(int[][] state) {
        this.state = new int[6][4];
        for(int i=0 ; i<6 ; i++){
            for(int j=0 ; j<4 ; j++){
                this.state[i][j] = state[i][j];
            }
        }
    }

    public void rotate(int plateNumber, boolean clockwise){
        mainRotate(plateNumber,clockwise);
        neighbourRotate(plateNumber,clockwise);
    }

    private void mainRotate(int plateNumber, boolean clockwise){
        int[] new_state = new int[4];
        if(clockwise){
            new_state[0] = state[plateNumber-1][2];
            new_state[1] = state[plateNumber-1][0];
            new_state[2] = state[plateNumber-1][3];
            new_state[3] = state[plateNumber-1][1];
        }
        else{
            new_state[0] = state[plateNumber-1][1];
            new_state[1] = state[plateNumber-1][3];
            new_state[2] = state[plateNumber-1][0];
            new_state[3] = state[plateNumber-1][2];
        }
        state[plateNumber-1] = new_state;
    }

    private void neighbourRotate(int plateNumber, boolean clockwise){
        int []neighbour = null;
        switch (plateNumber){
            case 1:
                neighbour = new int[]{6,4,3,2};
                break;
            case 2:
                neighbour = new int[]{1,3,5,6};
                break;
            case 3:
                neighbour = new int[]{1,4,5,2};
                break;
            case 4:
                neighbour = new int[]{6,5,3,1};
                break;
            case 5:
                neighbour = new int[]{2,3,4,6};
                break;
            case 6:
                neighbour = new int[]{2,5,4,1};
        }

        if(clockwise==false){
            int tmp;
            tmp = neighbour[0];
            neighbour[0] = neighbour[3];
            neighbour[3] = tmp;

            tmp = neighbour[1];
            neighbour[1] = neighbour[2];
            neighbour[2] = tmp;
        }

        Pair[] p = new Pair[4];
        for(int i=0 ; i<4 ; i++){
            p[i] = getPlateSide(neighbour[i],
                    relationPlate(plateNumber,neighbour[i]));
        }

        for(int i=0 ; i<4 ; i++){
            setPlateSide(neighbour[(i+1)%4],
                    relationPlate(plateNumber,neighbour[(i+1)%4]),
                    p[i]);
        }

    }

    private Pair getPlateSide(int plateNumber,Pair request){
        return new Pair(state[plateNumber-1][request.left],state[plateNumber-1][request.right]);
    }

    private void setPlateSide(int plateNumber, Pair request, Pair new_state){
        state[plateNumber-1][request.left] = new_state.left;
        state[plateNumber-1][request.right] = new_state.right;
    }

    private Pair relationPlate(int fP, int sP){
        switch (fP*10 + sP){
            case 10+2:
            case 10+3:
            case 10+4:
                return new Pair(1,0);
            case 10+6:
                return new Pair(2,3);
            case 20+1:
            case 20+3:
            case 20+5:
            case 20+6:
                return new Pair(0,2);
            case 30+1:
                return new Pair(2,3);
            case 30+2:
                return new Pair(3,1);
            case 30+4:
                return new Pair(0,2);
            case 30+5:
                return new Pair(1,0);
            case 40+1:
            case 40+3:
            case 40+5:
            case 40+6:
                return new Pair(3,1);
            case 50+2:
            case 50+3:
            case 50+4:
                return new Pair(2,3);
            case 50+6:
                return new Pair(1,0);
            case 60+1:
                return new Pair(1,0);
            case 60+2:
                return new Pair(0,2);
            case 60+4:
                return new Pair(3,1);
            case 60+5:
                return new Pair(2,3);
        }
        return null;
    }

    public int[][] getState() {
        return state;
    }


    public int[][] nextState(int number){
        int plateNumber = getPlateNumber(number);
        boolean clockwise = getClockwise(number);
        rotate(plateNumber,clockwise);
        return state;
    }

    public void interpret(int number){
        System.out.print("Plate #");
        System.out.print(getPlateNumber(number));
        System.out.print(" Rotate in ");
        if(getClockwise(number)){
            System.out.println("Clockwise.");
        }
        else{
            System.out.println("Counter Clockwise");
        }
    }

    public int getPlateNumber(int number){
        return number/2+1;
    }

    public boolean getClockwise(int number){
        return number%2==0;
    }

    public int reverseClockwise(int number) {
        if (number % 2 == 0) {
            number++;
        } else {
            number--;
        }
        return number;
    }

    public void showState(){
        System.out.println("       _____");
        System.out.println("      | "+state[0][0]+" "+state[0][1]+" |");
        System.out.println("      | "+state[0][2]+" "+state[0][3]+" |");
        System.out.println(" _____|_____|_____");
        System.out.println("| "+state[1][0]+" "+ state[1][1]+" | "+state[2][0]+" "+state[2][1]+" | "+state[3][0]+" "+state[3][1]+" | ");
        System.out.println("| "+state[1][2]+" "+ state[1][3]+" | "+state[2][2]+" "+state[2][3]+" | "+state[3][2]+" "+state[3][3]+" | ");
        System.out.println("|_____|_____|_____|");
        System.out.println("      | "+state[4][0]+" "+state[4][1]+" |");
        System.out.println("      | "+state[4][2]+" "+state[4][3]+" |");
        System.out.println("      |_____|");

        System.out.println("      | "+state[5][0]+" "+state[5][1]+" |");
        System.out.println("      | "+state[5][2]+" "+state[5][3]+" |");
        System.out.println("      |_____|");

        System.out.println("");
    }

    public boolean isGoal(){
        for(int i=0 ; i<6 ; i++){
            if(state[i][0] != state[i][1]
                    || state[i][1] != state[i][2]
                    || state[i][2] != state[i][3]){
                return false;
            }
        }
        return true;
    }

    public int[][][] getGoalStates(){
        goalStates = new int[720][6][4];
        numberGoalState = 0;
        permute(Arrays.asList(1,2,3,4,5,6), 0);
        return  goalStates;
    }

    private void permute(List<Integer> arr, int k){
        for(int i = k; i < 6; i++){
            java.util.Collections.swap(arr, i, k);
            permute(arr, k+1);
            java.util.Collections.swap(arr, k, i);
        }
        if (k == 6 -1){
            for(int i=0 ; i<6 ; i++){
                for(int j=0 ; j<4 ; j++){
                    goalStates[numberGoalState][i][j] = arr.get(i);
                }
            }
            numberGoalState++;

        }
    }

    public boolean isStatesEqual(int[][] state1,int[][] state2){
        for(int i=0 ; i<6 ; i++){
            for(int j=0 ; j<4 ; j++){
                if(state1[i][j] != state2[i][j]){
                    return false;
                }
            }
        }
        return true;
    }

}
