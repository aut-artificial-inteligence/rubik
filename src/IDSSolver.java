import java.util.ArrayList;

public class IDSSolver extends Solver{
    private final int INITIAL_DEPTH_LIMIT = 0;
    private final int MAXIMUM_DEPTH_LIMIT = 12;
    private Node root;
    private int memoryNodeAllocated;
    private int memoryNodeAllocatedMax;

    public IDSSolver(int[][] initialState){
        rubik = new Rubik();
        this.initialState = initialState;
    }

    public void solve(){
        int depthLimit = INITIAL_DEPTH_LIMIT;
        root = new Node(null,-1,initialState);
        ArrayList<Integer> solution;
        memoryNodeAllocatedMax = 0;

        while (depthLimit<=MAXIMUM_DEPTH_LIMIT){
            memoryNodeAllocated = 0;
            solution = IDS(root,depthLimit);
            if(memoryNodeAllocated > memoryNodeAllocatedMax){
                memoryNodeAllocatedMax = memoryNodeAllocated;
            }
            if(solution==null){
                depthLimit++;
            }
            else{
               showSolution(solution);
               return;
            }
        }
        System.out.println("Solver can not find path to goal!");
        return;
    }


    public ArrayList<Integer> IDS(Node parentNode, int depthLimit){
        memoryNodeAllocated++;
        if(memoryNodeAllocated>memoryNodeAllocatedMax){
            memoryNodeAllocatedMax = memoryNodeAllocated;
        }
        rubik.setState(parentNode.getState());
        if(rubik.isGoal()){
            memoryNodeAllocated--;
            return pathToRoot(parentNode);
        }
        if(depthLimit<=0){
            memoryNodeAllocated--;
            return null;
        }
        expandNodes++;
        for(int i=0 ; i<rubik.NUMBER_NEIGHBOUR ; i++){
            rubik.setState(parentNode.getState());
            int[][] nextState = rubik.nextState(i);
            Node childNode = new Node(parentNode,i,nextState);
            exploreNodes++;
            ArrayList<Integer> solution = IDS(childNode,depthLimit-1);
            if(solution!=null){
                memoryNodeAllocated--;
                return solution;
            }
        }
        memoryNodeAllocated--;
        return null;
    }

    @Override
    public void showSolutionMemory() {
        System.out.println("Maximum Node storage : "+ memoryNodeAllocatedMax);
    }
}
